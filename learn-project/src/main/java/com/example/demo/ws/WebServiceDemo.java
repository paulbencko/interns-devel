package com.example.demo.ws;

import com.example.demo.spring.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

/**
 * @author Tiberiu Pasat
 */
@Component
@Path("/demo")
public class WebServiceDemo {
    @Autowired private HelloWorldService helloWorldService;

    @Path("/")
    @GET
    public String hello(@QueryParam("name") String name) {
        return helloWorldService.hello(name);
    }
}
