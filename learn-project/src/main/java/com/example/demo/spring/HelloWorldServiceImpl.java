package com.example.demo.spring;

import org.springframework.stereotype.Service;

/**
 * @author Tiberiu Pasat
 */
@Service
public class HelloWorldServiceImpl implements HelloWorldService {

    @Override
    public String hello(String name) {
        return "Hello, " + name + "!";
    }
}
