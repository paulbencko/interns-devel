package com.example.springboot;

import com.sun.faces.config.ConfigureListener;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Configuration;

import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * @author Tiberiu Pasat
 */
@Configuration
public class JSFInitializer implements ServletContextInitializer {

    @Override
    public void onStartup(ServletContext ctx) throws ServletException {
        ctx.setInitParameter("javax.faces.PROJECT_STAGE", "Development");
        ctx.setInitParameter("javax.faces.HONOR_CURRENT_COMPONENT_ATTRIBUTES", "true");
        ctx.setInitParameter("javax.faces.STATE_SAVING_METHOD", "server");
        ctx.setInitParameter("javax.faces.DEFAULT_SUFFIX", ".xhtml");
        ctx.setInitParameter("javax.faces.FACELETS_SKIP_COMMENTS", "true");
        ctx.setInitParameter("com.sun.faces.validateXml", "true");
        ctx.setInitParameter("com.sun.faces.numberOfViewsInSession", "20");
        ctx.setInitParameter("com.sun.faces.numberOfLogicalViews", "50");
        ctx.setInitParameter("com.sun.faces.enableRestoreView11Compatibility", "true");
        ctx.setInitParameter("com.sun.faces.responseBufferSize", "128");
        ctx.setInitParameter("com.sun.faces.resourceUpdateCheckPeriod", "-1");

        ctx.addListener(new ConfigureListener());
        ServletRegistration.Dynamic facesServlet = ctx.addServlet("facesServlet", new FacesServlet());
        facesServlet.addMapping("*.jsf");
        facesServlet.setLoadOnStartup(1);
    }
}
