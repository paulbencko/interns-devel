package com.example.springboot;

import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * @author Tiberiu Pasat
 */
@Configuration
public class JerseyInitializer implements ServletContextInitializer {
    @Override
    public void onStartup(ServletContext ctx) throws ServletException {
        ServletRegistration.Dynamic fesJerseyServlet = ctx.addServlet("fesJerseyServlet", new SpringServlet());
        fesJerseyServlet.addMapping("/fes/*");
        fesJerseyServlet.setLoadOnStartup(1);

        fesJerseyServlet.setInitParameter("jersey.config.server.provider.packages", "com.example");
        fesJerseyServlet.setInitParameter("jersey.config.server.provider.classnames", "org.glassfish.jersey.jackson.JacksonFeature;com.example.springboot.JacksonContextResolver");
    }
}
